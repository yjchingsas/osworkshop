## These utilities are used for the EYAP demo

import csv
import os
import sys
import json
import inflection
import csv
import swat
import glob

from ..exceptions import ViyaException

import logging
logger = logging.getLogger(__name__)

def create_metadata(project, model, content_list, definition):
    metadata = {
        'projectId': definition.projectId,
        'projectName': project.Name,
        'modelId': model.Id,
        'modelName': model.Name,
        'taskId': definition.Id,
        ## this needs to be more intelligently done - can we have it look for the content_Type and use that to decide ?
        'trainCodeId': content_list[1].id,
        'scoreCodeId': content_list[2].id,
        'contentId': content_list[3].id,
        'targetVariable': project.targetVariable,
        'targetEventValue': project.targetEventValue,
        'nonEventValue': project.classTargetValues.replace(project.targetEventValue+',',""),
        'inputVariables': ', '.join(definition.inputVariables),
        'outputVariables': ', '.join(definition.outputVariables),
        'modelDisplayName': model.Name,
        'dataPrefix': definition.dataPrefix,
        'sequence': '',
        'timeValue': ''
        }
    return metadata

def create_metadata_csv(project, model, content_list, definition, path=os.getcwd(), filename='model_metadata'):
    metadata = create_metadata(project, model, content_list, definition)
    with open(path+'/'+filename+".csv",'w', newline='') as f:
        w = csv.writer(f)
        w.writerow(metadata.keys())
        w.writerow(metadata.values())
    return print('Metadata file %s created successfully!' % (filename) )

def create_requirements_file(package_list=[], path=os.getcwd(), file='requirements.txt'):
    with open(path+'/'+file, 'w') as fp:
        for package_name in package_list:
            exec("import {module}".format(module=package_name))
            package = sys.modules[package_name]
            if package_name == 'sklearn':
                package_name = 'scikit-learn'
            fp.writelines([
                package_name+'==%s\n' % package.__version__,
                      ])
    with open (path+'/'+file, 'r') as fp:
        print(fp.read())
    if not package_list:
        print('Please provide a list of packages.')


def load_to_viya(server,user,password,score_result_dir,dataPrefix, printCASMessages = False):
    #os.environ['CAS_CLIENT_SSL_CA_LIST']='/opt/sas/viya/config/etc/SASSecurityCertificateFramework/cacerts/trustedcerts.pem'
    s = swat.CAS(server, 5570, user, password)
    swat.options.cas.print_messages = printCASMessages
    perfFiles = glob.glob(score_result_dir+dataPrefix+'*')
    for i, file_ in enumerate(perfFiles, 1):
        filename = os.path.splitext(os.listdir(score_result_dir)[i])[0]
        upload_file = s.upload_file(file_, casout=filename)
        if s.table.tableExists(caslib='Public', name=filename).exists ==2:
            s.table.dropTable(caslib='Public', name=filename)
        promote_table = s.table.promote(caslib='CASUSER',
                                        drop=True,
                                        targetLib = 'Public',
                                        name=filename)
        results = s.table.tableExists(caslib='Public', name=filename)
        if results.exists == 0:
            print ('The table %s failed to load into memory.' % (filename))
        elif results.exists == 1:
            print ('Promotion failed, the table %s is session-scoped.' % (filename))
        else:
            print ('The table %s was successfully promoted!' % (filename))
    s.session.endsession()
