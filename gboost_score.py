import numpy as np
import pickle
import pandas as pd

def computeScore(LoanValue, MortgageDue, HomeValue, YearsOnJob, DerogatoryMarks, Delinquencies, CredLineAge, Inquiries, CredLines, DebtIncRatio):
    "Output: P_Default1, P_Default0"

    try:
        dm_model
    except NameError:       
        pfile = open('/data/jupyter/gboost_V3_6_8.pkl', 'rb')
        dm_model = pickle.load(pfile)
        pfile.close()

    input_list=[LoanValue, MortgageDue,HomeValue, YearsOnJob, DerogatoryMarks, Delinquencies, CredLineAge, Inquiries, CredLines, DebtIncRatio]
    converted = [0 if v is None else v for v in input_list]
    output_list = [(converted)]

    X = pd.DataFrame(output_list, columns=['LoanValue', 'MortgageDue','HomeValue', 'YearsOnJob', 'DerogatoryMarks', 'Delinquencies', 'CredLineAge', 'Inquiries', 'CredLines', 'DebtIncRatio'])
    P_Default0 = dm_model.predict_proba(X)[0,0]
    P_Default1 = dm_model.predict_proba(X)[0,1]

    return float(P_Default0), float(P_Default1)
