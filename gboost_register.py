## Register Python model assets to Model Manager

import sys
import os
from viyapy import ViyaClient
from viyapy.variables import String, Decimal, Boolean, Date, DateTime, Integer

import logging
logger = logging.getLogger('viyapy')
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.INFO)

protocol=os.environ['PROTOCOL']
server = os.environ['SERVER']
user = os.environ['USER']
password = os.environ['PASSWORD']

password = password.replace("z","")

client = ViyaClient(protocol + server)
token = client.logon.authenticate(user, password)

repositoryName = 'DMRepository'
repository = client.model_manager.get_repository(repositoryName)

project = repository.create_project(
    name='ModelOps_Bitbucket',
    description='Project demonstrating API registration',
    function='Classification',
    target_level='binary',
    target_event_value='1',
    class_target_values='1,0',
    target_variable='Default',
    event_probability_variable='P_Default1',
    external_url='http://'+server+'/ModelOpsPythonRegister.ipynb',
    input=[
        Decimal('DebtIncRatio'),
        Decimal('LoanValue'),
        Decimal('MortgageDue'),
        Decimal('HomeValue'),
        Decimal('YearsOnJob'),
        Decimal('DerogatoryMarks'),
        Decimal('Delinquencies'),
        Decimal('CredLines'),
        Decimal('CredLineAge'),
        Decimal('Inquiries')
    ],
    output=[
        Decimal('P_Default0', description='Probability of not defaulting'),
        Decimal('P_Default1', description='Probability of defaulting')
    ])

model = project.create_model(
    name='Python_GradientBoost',
    algorithm='scikit-learn.GradientBoostingClassifier',
    modeler='Yi Jian Ching',
    files=[
        'gboost_train.py',
        'gboost_score.py',
        ('model/gboost_V3_6_8.pkl', True)
    ])

perf_task = project.create_performance_definition(
    name='Performance Monitoring',
    models=[model])

metadata = {
    'projectId': project.Id,
    'projectName': project.Name,
    'modelId': model.Id,
    'modelName': model.Name,
    'taskId': perf_task.Id,
    'contentId': '',
    'targetVariable': project.targetVariable,
    'targetEventValue': project.targetEventValue,
    'nonEventValue': project.classTargetValues.replace(project.targetEventValue+',',""),
    'inputVariables': ', '.join(perf_task.inputVariables),
    'outputVariables': ', '.join(perf_task.outputVariables),
    'modelDisplayName': model.Name,
    'dataPrefix': perf_task.dataPrefix,
    'sequence': '',
    'timeValue': ''
}

import csv
with open(os.getcwd() + "/model/gboost_metadata.csv",'w', newline='') as f:
    w = csv.writer(f)
    w.writerow(metadata.keys())
    w.writerow(metadata.values())
