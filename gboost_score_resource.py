import pickle
import pandas as pd

def score(input_df, model):
    """
    :param input_df: pandas.DataFrame
           model: scikit-learn.GradientBoostingClassifier
    :return: pandas.DataFrame
    """

    inputs = input_df.drop(['Default'], axis=1)
    proba =  model.predict_proba(inputs.fillna(inputs.median()))

    return pd.DataFrame(proba, columns=('P_Default0', 'P_Default1'))
