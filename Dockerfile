#
# Copyright 2018 SAS Institute Inc.
#
# This work is licensed under a Creative Commons Attribution 4.0 International License.
# You may obtain a copy of the License at https://creativecommons.org/licenses/by/4.0/ 
#
# docker push docker.sas.com/ap_pyscore_base:0.1
#
FROM docker.sas.com/ap_pyscore_base:latest
MAINTAINER Neal Lee "neal.lee@sas.com"

COPY model/gboost_obj_3_6_1.pkl /home/sasdemo/score/model/gboost_obj_3_6_1.pkl
COPY model/gboost_obj_3_6_1.pkl /model/gboost_obj_3_6_1.pkl
COPY gboost_score.py /home/sasdemo/score/gboost_score.py
COPY gboost_register.py /home/sasdemo/score/gboost_register.py
COPY gboost_test.py /home/sasdemo/score/gboost_test.py
COPY gboost_train.py /home/sasdemo/score/gboost_train.py
ENTRYPOINT [ "/usr/bin/tini", "--" ]
CMD [ "/bin/bash" ]
