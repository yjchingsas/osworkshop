import unittest
import os
import pandas as pd
import numpy as np
import pickle
import os

def computeScore(LoanValue, MortgageDue, HomeValue, YearsOnJob, DerogatoryMarks, Delinquencies, CredLineAge, Inquiries, CredLines, DebtIncRatio):
    "Output: P_Default1, P_Default0"

    try:
        dm_model
    except NameError:       
        pfile = open(os.getcwd()+'/model/gboost_V3_6_8.pkl', 'rb')
        dm_model = pickle.load(pfile)
        pfile.close()

    input_list=[LoanValue, MortgageDue,HomeValue, YearsOnJob, DerogatoryMarks, Delinquencies, CredLineAge, Inquiries, CredLines, DebtIncRatio]
    converted = [0 if v is None else v for v in input_list]
    output_list = [(converted)]

    X = pd.DataFrame(output_list, columns=['LoanValue', 'MortgageDue','HomeValue', 'YearsOnJob', 'DerogatoryMarks', 'Delinquencies', 'CredLineAge', 'Inquiries', 'CredLines', 'DebtIncRatio'])
    P_Default0 = dm_model.predict_proba(X)[0,0]
    P_Default1 = dm_model.predict_proba(X)[0,1]

    return float(P_Default0), float(P_Default1)

class Test(unittest.TestCase):
    def test(self):
        self.assertEqual(computeScore(1100,25680,39025,10.5,2,2,94.36667,1,9,37)
, (0.09789084404563342, 0.9021091559543666))

if __name__ == '__main__':
    unittest.main()
